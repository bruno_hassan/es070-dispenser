/*
FILE NAME:  board.h
FILE DESCRIPTION: definição dos pinos que serão utilizados na placa ESP32

AUTHORS:
        Bruno Hassan - 165240
        Danilo Polly - 166387
        Leonardo de Souza - 178663
        Leonardo Vallin - 160190

CREATED: 02/10/2021
MODIFIED: 08/10/2021
*/
#ifndef SOURCES_BOARD_H_
#define SOURCES_BOARD_H_

//            Sonar HC-SR04 (aproximacao da mao)
#define HAND_SONAR_TRIG_PIN                     13
#define HAND_SONAR_ECHO_PIN                     12
#define HAND_SONAR_MAX_DISTANCE                 30
#define HAND_MIN_DISTANCE                       15

//            Sonar HC-SR04 (altura do alcool)
#define ALCOHOL_SONAR_TRIG_PIN                  33
#define ALCOHOL_SONAR_ECHO_PIN                  32
#define ALCOHOL_SONAR_MAX_DISTANCE              30
#define ALCOHOL_MIN_DISTANCE                    25

//            Relé Bomba Água
#define RELE_PUMP_PIN                           34

//            Buzzer
#define BUZZER_PIN                              14

//            LED UV
#define UV_PIN                                  35

//            LED UV
#define RFID_SDA_PIN                            5
#define RFID_SCK_PIN                            18
#define RFID_MOSI_PIN                           23
#define RFID_MISO_PIN                           19
#define RFID_RST_PIN                            22

//            LCD - LCM1602 IIC
#define LCD_SDA_PIN                             21
#define LCD_SCL_PIN                             22
#define LCD_ADDRESS                             0x27        //TODO: ATUALIZAR

//            Temperatura MLX90614
#define TEMP_SDA_PIN                            21
#define TEMP_SCL_PIN                            22
#define MAX_ACCEPTED_TEMP                       36

#endif
