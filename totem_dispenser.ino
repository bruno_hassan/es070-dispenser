/*
UNIVERSIDADE ESTADUAL DE CAMPINAS - ES070 - LAB EMBARCADOS

FILE NAME:  totem_dispenser.ino
FILE DESCRIPTION: 
        Arquivo principal da solucao de embarcado para o sistema de dispenser de alcool inteligente e conectado.
        Hardware escolhido:
            - sonar HR-SR04
            - sensor temperatura MLX90614
            - display LCD 16x2 com modulo LCM1602 IIC
            - led UV, verde e vermelho
            - rele para a bomba hidraulica
            - buzzer
            - RFID [NAO IMPLEMENTADO]

HARDWARE IMPROVEMENTS:
    - integrar sistema de comunicacao entre totem e area admistrativa do estabelecimento (1 via)
    - implementar botao de ajuda para usuario

ALGORITHM IMPROVEMENTS:
    - melhorar log de pessoas no dia, usando RFID como credencial
    - verificar se existe alcool no recipiente antes de acionar a bomba
    - pesquisa por temperatura ideal na medicao do pulso de usuarios

CODE IMPROVEMENT:
    - usar interrupcao no lugar de delay para desligamento do UV e da bomba
    - criar classe para armazenar funcoes do webserver
    - melhorar UX da interface do webserver

AUTHORS:
        Bruno Hassan - 165240
        Danilo Polly - 166387
        Leonardo de Souza - 178663
        Leonardo Vallin - 160190

CREATED: 02/10/2021
MODIFIED: 19/11/2021
*/

// importacao de bibliotecas
#include <Adafruit_MLX90614.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <LinkedList.h>
#include "board.h"
#include "sonar.h"
#include "display_lcd.h"
#include "bomba.h"
#include "buzzer.h"
#include "webserver.h"
#include "wifi_credentials.h"

// definicao constantes
const int iLcdColumns = 16;
const int iLcdRows = 2;

// definicao variaveis globais e objetos
int iQntPessoasLiberadas = 0,
    iQntPessoasBarradas = 0;
float fAlturaAlcool = 1; // altura do recipiente em porcentagem
String webpage;
LinkedList<float> llTemperatura;

extern WebServer server;
Adafruit_MLX90614 temp_sensor = Adafruit_MLX90614();
DisplayLCD lcd(LCD_ADDRESS, iLcdColumns, iLcdRows);     // TODO: BUSCAR QUAL EH O ENDERECO DO MODULO (https://randomnerdtutorials.com/esp32-esp8266-i2c-lcd-arduino-ide/)
Sonar handSonar(HAND_SONAR_TRIG_PIN, HAND_SONAR_ECHO_PIN, HAND_SONAR_MAX_DISTANCE);
Sonar alcoholSonar(ALCOHOL_SONAR_TRIG_PIN, ALCOHOL_SONAR_ECHO_PIN, ALCOHOL_SONAR_MAX_DISTANCE);

void handleRoot() {
    server.send(200, "text/html", html(iQntPessoasLiberadas, iQntPessoasBarradas, fAlturaAlcool, llTemperatura));
}

void reset(){
    iQntPessoasLiberadas = 0;
    iQntPessoasBarradas = 0;
    LinkedList<float> llTemperatura;
    server.send(200, "text/plain", "Variaveis do sistema resetadas com sucesso!")
}

void setup() {
    Serial.begin(115200); // configurada serial para visualizacao de logs

    // conexao do webserver
    Serial.print("Se conectando a: ");
    Serial.println(SSID);
    
    WiFi.mode(WIFI_STA);
    WiFi.begin(SSID, PASSWORD);
    
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.print(".");
    }
    Serial.println("\nConectado");
    Serial.print("IP: ");
    Serial.println(WiFi.localIP());
    
    server.on("/", handleRoot);
    server.on("/reset", reset);
//    server.on("/get_info", get_access_number);    // rota para retornar informacoes do dia
    server.onNotFound(handleNotFound);
    server.begin();

    lcd.init();
    temp_sensor.begin();
    pinMode(BUZZER_PIN, OUTPUT);
    pinMode(RELE_PUMP_PIN, OUTPUT);
    pinMode(UV_PIN, OUTPUT);
    digitalWrite(UV_PIN, LOW);
}

void loop(){
    float fTemp;
    unsigned int uiHandDistance, uiAlcoholDistance;
    String sDisplayMessage;
    bool bAccessGranted = 0;

    server.handleClient();
    delay(1); // espera para a cpu mudar para prox tarefa

    uiHandDistance = handSonar.getDistance();
    if(uiHandDistance <= HAND_MIN_DISTANCE){
        // caso usuario tenha aproximado a mao, liga a luz uv e eh medida sua temperatura
        digitalWrite(UV_PIN, HIGH);
        fTemp = temp_sensor.readObjectTempC();
        llTemperatura.add(fTemp);

        if (fTemp > MAX_ACCEPTED_TEMP){
            digitalWrite (BUZZER_PIN, HIGH);
            delay(1500);
            digitalWrite (BUZZER_PIN, LOW);
            bAccessGranted = 0; // False
        } else {
            bAccessGranted = 1; // True
        }

        // o rele eh acionado, ativando a bomba do alcool
        digitalWrite(RELE_PUMP_PIN, HIGH);
        delay(200);
        digitalWrite(RELE_PUMP_PIN, LOW);

        // apagar uv
        delay(2000);
        digitalWrite(UV_PIN, LOW);

        // liberacao do acesso (catraca)
        if (bAccessGranted){
            lcd.accessGranted(fTemp);
            iQntPessoasLiberadas++;
            //TODO acender led verde?
        } else {
            lcd.accessDenied(fTemp);
            iQntPessoasBarradas++;
            //TODO acender led vermelho?
        }

        // sonar posicionado em cima do recipiente de modo que quanto maior a distancia, menor a quantidade de alcool
        uiAlcoholDistance = alcoholSonar.getDistance();
        fAlturaAlcool = 1 - (uiAlcoholDistance/ALCOHOL_SONAR_MAX_DISTANCE);
    }
}
