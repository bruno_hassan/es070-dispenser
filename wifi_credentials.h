/*
FILE NAME:  wifi_credentials.h
FILE DESCRIPTION: definicao das credenciais para criacao do webserver

AUTHORS:
        Bruno Hassan - 165240
        Danilo Polly - 166387
        Leonardo de Souza - 178663
        Leonardo Vallin - 160190

CREATED: 13/10/2021
MODIFIED: 15/10/2021
*/

const char* SSID = "YOUR_SSID";
const char* PASSWORD = "YOUR_PASSWORD";

