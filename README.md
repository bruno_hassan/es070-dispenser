## Dispenser Inteligente e conectado

Dispositivo automático que previne a entrada de pessoas possivelmente infectadas com COVID-19 em ambientes comerciais e desinfecta as mãos do usuário por meio do dispenser de álcool e raios UV.

**Desenvolvimento feito no Arduino IDE com a configuração para ESP32**

**Hardware utilizado:**

* placa ESP32

* sonar HR-SR04

* sensor temperatura MLX90614

* display LCD 16x2 com modulo LCM1602 IIC

* led UV

* rele para a bomba hidraulica

* buzzer

**Bibliotecas utilizadas:**

* NewPing

* Adafruit_MLX90614

* LiquidCrystal_I2C

* LinkedList

* WiFi


---
*Para a configuração do ambiente para o desenvolvimento seguir tutorial no site do [UsinaInfo](https://www.usinainfo.com.br/blog/programar-esp32-com-a-ide-arduino-tutorial-completo/)*

*Para adicionar as bibliotecas utilizadas presentes na raiz do projeto à IDE selecionar **Sketch**>**Incluir Biblioteca**>**Adicionar biblioteca .ZIP** e selecionar as bibliotecas existentes. Outra opção é o download nativo da plataforma em **Sketch**>**Incluir Biblioteca**>**Gerenciar Bibliotecas** e buscar pelo nome dos requisitos na barra de pesquisa.*

*Antes de fazer o upload do código na placa, alterar as informações no arquivo **wifi_credentials** para que o webserver seja construído corretamente.*